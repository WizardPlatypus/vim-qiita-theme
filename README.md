Just wanna to share my simple Qiita-like theme for Vim because i didn't found anything like this yet and i spend whole day managing how to make it.

To use it just copy Qiita-theme.vim file into your .vim/colors/ folder.

P.S.: you probably are going to need background color for your terminal/gvim (whatever), so here what i use: #415358(the darker one) and  #576f75.

