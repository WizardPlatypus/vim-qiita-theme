set bg=dark
hi clear
if exists("syntax_on")
    syntax reset
endif

let g:colors_name="Qiita-theme"

"" Commons
hi Normal ctermfg=231 ctermbg=none guibg=#364549 guifg=#e3e3e3
hi ErrorMsg ctermfg=210 ctermbg=none
hi visual ctermbg=0
hi Todo ctermfg=210 ctermbg=none cterm=bold guifg=#ebd247
hi Search ctermfg=210 ctermbg=66
hi IncSearch ctermfg=210 ctermbg=66

"" Toolbar
hi ToolbarButton guibg=#2c383a guifg=#e3e3e3 gui=none
hi ToolbarLine guibg=#2c383a guifg=#e3e3e3 gui=none

"" Specials
hi SpecialKey ctermfg=244 guifg=#576f75 gui=none
hi SpecialChar ctermfg=220 guifg=#576f75 gui=none
hi SpecialComment ctermfg=248
hi Directory ctermfg=253
hi Title ctermfg=231 cterm=bold
hi WarningMsg ctermfg=231 ctermbg=none cterm=undercurl guisp=#df4f4f
hi ModeMsg ctermfg=231
hi Question ctermfg=231 cterm=none guifg=#8bdf4c
hi NonText ctermfg=254 guifg=#ffffff gui=none

"" Menu
hi Menu ctermfg=253
hi WildMenu ctermfg=253 ctermbg=238 cterm=none

hi TabLine cterm=none ctermbg=0 ctermfg=254 guibg=#2c383a gui=none
hi TabLineSel cterm=none ctermbg=0 ctermfg=254 guibg=#364549 gui=none
hi TabLineFill ctermbg=0 ctermfg=none cterm=none term=none gui=none guibg=#2c383a

"" Status lines
hi StatusLine ctermfg=231 ctermbg=70 cterm=none guibg=#55c500 guifg=#ffffff gui=none
hi StatusLineNC ctermfg=231 ctermbg=66 cterm=none guibg=#576f75 guifg=#ffffff gui=none
hi StatusLineTerm ctermfg=231 ctermbg=70 cterm=none guibg=#55c500 guifg=#ffffff gui=none
hi StatusLineTermNC ctermfg=231 ctermbg=66 cterm=none guibg=#576f75 guifg=#ffffff gui=none

hi VertSplit ctermfg=231 ctermbg=0 cterm=none

hi Folded ctermfg=248 ctermbg=none cterm=bold
hi FoldColumn ctermfg=248 ctermbg=none cterm=bold
hi SignColumn ctermbg=none

hi LineNr ctermfg=248 ctermbg=0 cterm=none guifg=#e3e3e3 guibg=#2c383a
hi CursorLineNr ctermfg=254 ctermbg=0 cterm=bold guifg=#ffffff guibg=#2c383a
hi CursorLine ctermbg=0 cterm=none guibg=#2c383a
hi MatchParen ctermfg=231 ctermbg=66 guibg=#576f75 guifg=#ffffff

"" Diffs
hi DiffAdd ctermfg=220 ctermbg=none cterm=none guifg=#88df4c guibg=#576f75
hi DiffChange ctermfg=78  ctermbg=none cterm=none guifg=#ebd247 guibg=#576f75
hi DiffText ctermfg=208 ctermbg=none cterm=none guifg=#9dabae guibg=#576f75
hi DiffDelete ctermfg=210 ctermbg=none cterm=none guifg=#ff8095 guibg=#576f75

"" Style
hi Bold cterm=bold gui=bold
hi Underlined cterm=underline gui=underline
hi Italic cterm=italic gui=italic
hi Ignore cterm=none gui=none
hi Error ctermfg=none ctermbg=none cterm=undercurl guisp=#ff8095 gui=none

"" Literals
hi Comment ctermfg=248 guifg=#9dabae
hi String ctermfg=210 guifg=#ff8095
hi Constant ctermfg=177 guifg=#ae81ff
hi Character ctermfg=183 guifg=#ebd247
hi Number ctermfg=141 guifg=#a980f5
hi Boolean ctermfg=111 guifg=#41b7d7
hi Float ctermfg=111 guifg=#a980f5
hi Tag ctermfg=210 guifg=#ff8095

"" Indentifier
hi Identifier ctermfg=231 cterm=none guifg=#e3e3e3 gui=none
hi Function ctermfg=112 cterm=none guifg=#8bdf4c gui=italic
hi Operator ctermfg=210 cterm=none guifg=#ebd247 gui=none
hi Type ctermfg=81  cterm=italic guifg=#41b7d7 gui=italic
hi StorageClass ctermfg=220 cterm=none guifg=#41b7d7 gui=italic

"" Keyword
hi Statement ctermfg=220 guifg=#ebd257 gui=none
hi Conditional ctermfg=220 guifg=#ebd257
hi Repeat ctermfg=220 guifg=#ebd257
hi Label ctermfg=220 guifg=#ebd257
hi Keyword ctermfg=220 guifg=#ebd257
hi Exception ctermfg=210 guifg=#ebd257

"" Spell Check
hi SpellBad    ctermfg=none ctermbg=none cterm=undercurl guisp=#ff8095
hi SpellCap    ctermfg=none ctermbg=none cterm=undercurl guisp=#ff8095
hi SpellLocal  ctermfg=none ctermbg=none cterm=undercurl guisp=#ff8095

